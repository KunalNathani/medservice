class MobileMenu {
    constructor() {
        //1.Store all the needed DOM elements in Object
        this.menuIcon = document.querySelector('.mobile-header-icon');
        this.mobileHeader = document.querySelector("#mobile-header");

        //2. Can create any class/object variables which are needed
        this.x = 10;

        //3. Call a function which registers all the events
        this.events();
    }

    events() {
        this.menuIcon.addEventListener('click', () => this.toggleMenu())
        // => operator preserves the 'this' object i.e here it will preserve the object of MobileMenu class
        // if we hadn't used the arrow operator this would be the object of the element which triggered the event
    }
    toggleMenu() {
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-header-icon-close");
    }
}

export default MobileMenu;
// so that we can import this file elsewhere as by default it is private
